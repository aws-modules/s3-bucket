resource "aws_s3_bucket" "this" {
  bucket = var.application != null ? "${var.environment}-${var.product}-${var.application}-${var.use_case}" : "${var.environment}-${var.product}-${var.use_case}"
  acl    = var.bucket_acl

  versioning {
    enabled = var.versioning
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = var.application != null ? {
    Environment             = var.environment
    Product                 = var.product
    Application             = var.application
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
    } : {
    Environment             = var.environment
    Product                 = var.product
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  bucket                  = aws_s3_bucket.this.id
  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets
}